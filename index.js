// import Vue from "vue";
import VueEchart, { echarts } from "./src/components/VueEchart";
import CircularEyeProgress from "./src/components/CircularEyeProgress";
import CircularProgress from "./src/components/CircularProgress";
import PictorialProgress from "./src/components/PictorialProgress";
import SpectrumProgress from "./src/components/SpectrumProgress";
import WarningProgress from "./src/components/WarningProgress";
import LiquidChart from "./src/components/LiquidChart";
import LegendList from "./src/components/LegendList";

const components = {
  VueEchart,
  LegendList,
  CircularEyeProgress,
  CircularProgress,
  PictorialProgress,
  SpectrumProgress,
  WarningProgress,
  LiquidChart
};
const install = function(Vue) {
  Vue.prototype.$echarts = echarts;
  for (const name in components) {
    Vue.component(name, components[name]);
  }
};

if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}
export default VueEchart;

export {
  echarts,
  VueEchart,
  CircularEyeProgress,
  CircularProgress,
  PictorialProgress,
  SpectrumProgress,
  WarningProgress,
  LiquidChart
};
